## MACHETE DE COMANDOS DE LINUX ##

Este machete esta basado en la version de ingles "Linux Commands Cheat Sheet" proporcionada por la página LinOxide.  
Se le agregaron algunos comandos básicos sobre instalación de paquetes y manejo de archivos y se modificaron otros para apuntar a sistemas basados en Debian en vez de Red Hat.  
Algunos comandos de red no estan testeados y hay que modificarlos a versiones mas nuevas.

*(ᴐ) Darío Abal*  
